/*
 * Please see the included README.md file for license terms and conditions.
 */


// This file is a suggested starting place for your code.
// It is completely optional and not required.
// Note the reference that includes it in the index.html file.


/*jslint browser:true, devel:true, white:true, vars:true */
/*global $:false, intel:false app:false, dev:false, cordova:false */



// This file contains your event handlers, the center of your application.
// NOTE: see app.initEvents() in init-app.js for event handler initialization code.

// function myEventHandler() {
//     "use strict" ;
// // ...event handler code here...
// }


// ...additional event handlers here...

window.Pascal = {
    isSintax: function(code) {
        if(code.search("program") == 0){
            if(code.search("uses crt;")) {
                if(code.search("begin")) {
                    if(code.search("end.")) {
                        $('#runResult').html(write("Hello World!")).css("color","#FFFFFF")
                    } else {
                        $('#runResult').html('Error in sintax! line:' + code.search("end.")).css("color","#FF0000");
                    }
                } else {
                    $('#runResult').html('Error in sintax! line:' + code.search("begin")).css("color","#FF0000");
                }
            } else {
                $('#runResult').html('Error in sintax! line:' + code.search("uses crt;")).css("color","#FF0000");
            }
        } else {
            $('#runResult').html('Error in sintax! line:' + code.search("program")).css("color","#FF0000");
        }
    }
}