/*jshint browser:true */
/*global $ */(function()
{
 "use strict";
 /*
   hook up event handlers 
 */
 function register_event_handlers()
 {
    
    
     /* button  Button */
    $(document).on("click", ".uib_w_1", function(evt)
    {
         /*global activate_page */
         activate_page("#uib_page_1"); 
         return false;
    });
    
        /* button  #btnMenuHome, #closeMenu */
    $(document).on("click", "#btnMenuHome, #closeMenu", function(evt)
    {

         uib_sb.toggle_sidebar($("#menu"));  
         return false;
    });
    
        /* button  .uib_w_6 */
    $(document).on("click", ".uib_w_6", function(evt)
    {
         /*global activate_page */
         uib_sb.toggle_sidebar($("#menu")); 
         activate_page("#mainpage"); 
         return false;
    });
    
        /* listitem  .uib_w_3 */
    $(document).on("click", ".uib_w_3", function(evt)
    {
         /*global activate_page */
         activate_page("#edit-rum"); 
        activate_subpage("#edit");
         return false;
    });
    
        /* button  #btnMenuEditRun */
    $(document).on("click", "#btnMenuEditRun", function(evt)
    {
         uib_sb.toggle_sidebar($("#menu"));  
         return false;
    });
    
        /* button  #btnEdit */
    $(document).on("click", "#btnEdit", function(evt)
    {
         /*global activate_subpage */
         activate_subpage("#edit"); 
         return false;
    });
    
        /* button  #btnRun */
    $(document).on("click", "#btnRun", function(evt)
    {
         /*global activate_subpage */
        Pascal.isSintax($('#textCode').val());
         activate_subpage("#run"); 
         return false;
    });
    
    }
 document.addEventListener("app.Ready", register_event_handlers, false);
})();
